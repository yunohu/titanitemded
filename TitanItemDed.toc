## Interface: 90001
## Title: Titan Panel [|cffeda55fItemized Deductions|r] |cff00aa009.0.1.0|r
## Notes: Drop the cheapest item from your bags and other bag space freeing functions. Never have a full bag error again.
## Version: 9.0.1.0
## X-Date: 2020-10-30
## Author: Kernighan
## X-Email:
## X-Website:
## X-Category:
## SavedVariables: TitanItemDedSettings, TPIDCache
## OptionalDeps: Auctioneer, Informant
## Dependencies: Titan
TitanItemDed.xml
